Pod::Spec.new do |s|  
  s.name     = 'DXPopver'  
  s.version  = '0.0.1'  
  s.license  = 'MIT'  
  s.summary  = 'A Popover mimic Facebook app popover using UIKit.'  
  s.homepage = 'http://git.oschina.net/JokerV/DXPopver'  
  s.author   = { 'JokerV' => '43114620@163.com' }  
  s.source   = { :git => 'hhttps://git.oschina.net/JokerV/DXPopver.git', :tag => s.version.to_s }  
  s.platform = :ios 
  s.source_files = 'DXPoppver.{h,m}'   
  
  s.requires_arc = true    
end  